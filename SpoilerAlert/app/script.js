﻿document.addEventListener('DOMContentLoaded', function () {
    var link = document.getElementById('buttons');
    var spoilers = [];

    $('table tbody').on("click", ".remove", function () {
        $(this).closest("tr").remove();

        debugger;
        let removeId = $(this).data("name");
        let indexToRemove = spoilers.indexOf(removeId);
        spoilers.splice(indexToRemove, 1);

        saveSpoilers();
    });

    let addNewButton = function (buttonText) {
        let markup = `<tr><td>${buttonText}</td><td><button class="btn btn-danger remove" data-name="${buttonText}">Remove</button></td></tr>`
        $("table tbody").append(markup);
    };

    let saveSpoilers = function () {
        let data = {
            "keyWords": spoilers
        };

        chrome.storage.sync.set(data, function () {

        });
    };

    chrome.storage.sync.get("keyWords", function (items) {
        for (var i = 0; i < items.keyWords.length; i++) {
            spoilers.push(items.keyWords[i]);
            addNewButton(items.keyWords[i]);
        }
    });

    // onClick's logic below:
    link.addEventListener('click', function () {
        let text = $("#spoilerInput").val();
        spoilers.push(text);
        addNewButton(text);
        saveSpoilers();

    });

});




