/// <reference path="inboxsdk.js" />
InboxSDK.load('1.0', 'sdk_SpoilerAlert_91e7c0cb0c').then(function (sdk) {
    sdk.Lists.registerThreadRowViewHandler(function (thread) {
        var subject = thread.getSubject().toLowerCase();
        chrome.storage.sync.get("keyWords", function (items) {
            for (var i = 0; i < items.keyWords.length; i++) {
                var text = items.keyWords[i].trim().toLowerCase();
                console.log(subject + " | " + text);
                if (subject.trim().toLowerCase().indexOf(text) > -1) {
                    for (var i = 0 ; i < 80; i++) {
                        thread.addLabel("Spoiler");
                    }
                }
            }
        });
    });
});



